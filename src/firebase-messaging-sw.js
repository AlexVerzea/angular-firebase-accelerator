importScripts("https://www.gstatic.com/firebasejs/5.1.0/firebase-app.js")
importScripts("https://www.gstatic.com/firebasejs/5.1.0/firebase-messaging.js")

firebase.initializeApp({
    apiKey: "AIzaSyCUqiEDIc0VbM-xBMNVM8fnUoZZyOSE8AI",
    authDomain: "fir-starter-6ab95.firebaseapp.com",
    databaseURL: "https://fir-starter-6ab95.firebaseio.com",
    projectId: "fir-starter-6ab95",
    storageBucket: "fir-starter-6ab95.appspot.com",
    messagingSenderId: "1074179928649"
});

const messaging = firebase.messaging();
import { Component, OnInit } from '@angular/core';
import { GeofireService } from '../../core/geofire/geofire.service';
import { Observable, interval } from 'rxjs';
import { tap, map, take, finalize } from 'rxjs/operators';
import { GeoFireCollectionRef } from 'geofirex';

@Component({
  selector: 'app-geoquery',
  templateUrl: './geoquery.component.html',
  styleUrls: ['./geoquery.component.scss']
})
export class GeoqueryComponent implements OnInit {

  constructor(private geofireService: GeofireService) {
    window.onbeforeunload = () => {
      this.collection.delete(this.docId);
    };
   }

  points: Observable<any>; // Holds the documents that we query.
  defaultLatitude = 45.50;
  defaultLongitude = -73.80;
  geo;
  center;
  testDoc;
  collection: GeoFireCollectionRef;
  clicked;
  docId;

  /**
     * Several things are needed for a geoquery:
     * 1. Center point, with latitude and longitude.
     * 2. Radius around the center point in km. Under the hood, GeoFireX uses Turf.js to calculate the
     * geodesic distance with the Haversine formula.
     * 3. The object that contains the geodata. A single document can have multiple geopoints that can be
     * queried against.
     */
  ngOnInit() {
    this.geo = this.geofireService.geofireInit();
    this.collection = this.geo.collection('positions');
    this.collection.setPoint('point1', 'pos', this.defaultLatitude + 0.2, this.defaultLongitude - 0.2);
    this.collection.setPoint('point2', 'pos', this.defaultLatitude - 0.2, this.defaultLongitude + 0.2);
    this.collection.setPoint('point3', 'pos', this.defaultLatitude + 0.2, this.defaultLongitude + 0.2);
    this.collection.setPoint('point4', 'pos', this.defaultLatitude - 0.2, this.defaultLongitude - 0.2);
    this.center = this.geo.point(this.defaultLatitude, this.defaultLongitude);
    this.points = this.collection.within(this.center, 100000, 'pos');
    this.testDoc = this.points.pipe(
      map(arr => arr.find(o => o.id === this.docId))
    );
  }

  // Simulate movement through test data generation (random latitude and longitude).
  start() {
    this.clicked = true;
    let testLatitude = 45.50 + this.rand();
    let testLongitude = -73.80 + this.rand();
    const randA = this.rand();
    const randB = this.rand();
    this.docId = 'testPoint' + Date.now();
    // let pointNumber = 0;
    interval(1000) // Time difference between two test points.
      .pipe(
        take(20), // Number of test points.
        tap(v => {
          testLatitude += randA * Math.random();
          testLongitude += randB * Math.random();
          const point = this.geo.point(testLatitude, testLongitude);
          const isInRadius = false;
          // if (Math.abs(Math.abs(testLatitude) - 45.50) >= 1 || Math.abs(Math.abs(testLongitude) - 73.80) >= 1) {
          //   isInRadius = false;
          // }
          const data = {
            // name: 'testPoint' + pointNumber,
            name: 'testPoint',
            recipientId: '47N9u6tCSiWjtBBcpsg2dJlUqbA3',
            pos: point.data,
            allow: true,
            isInRadius: isInRadius
          };
          this.collection.setDoc(this.docId, data);
        }),
        finalize(() => {
          this.clicked = false;
          this.collection.delete(this.docId);
        })
      )
      .subscribe();
  }

  trackByFn(_, doc) {
    return doc.id;
  }
  icon(id) {
    return id.includes('testPoint') ? 'https://goo.gl/dGBkRz' : null;
  }

  rand() {
    const arr = [0.15, -0.15];
    return arr[Math.floor(Math.random() * arr.length)];
  }

}

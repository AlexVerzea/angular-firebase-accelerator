import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeoqueryComponent } from './geoquery.component';

describe('GeoqueryComponent', () => {
  let component: GeoqueryComponent;
  let fixture: ComponentFixture<GeoqueryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeoqueryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeoqueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

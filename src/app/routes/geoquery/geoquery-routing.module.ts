import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeoqueryComponent } from './geoquery.component';
import { RouterModule, Routes } from '@angular/router';

// Angular Google Maps
import { AgmCoreModule } from '@agm/core';

const geoqueryRoutes: Routes = [
  { path: '', component: GeoqueryComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(geoqueryRoutes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDmXKGn8KyMo8cUk9E4FXvRfiAIG9iwTKI'
    })
  ],
  exports: [
    RouterModule
  ],
  declarations: [
    GeoqueryComponent
  ]
})
export class GeoqueryRoutingModule { }

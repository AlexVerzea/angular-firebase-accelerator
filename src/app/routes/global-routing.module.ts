import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const globalRoutes: Routes = [
  { path: '', loadChildren: './custom-sign-in/custom-sign-in-routing.module#CustomSignInRoutingModule' },
  { path: 'google-sign-in', loadChildren: './google-sign-in/google-sign-in-routing.module#GoogleSignInRoutingModule' },
  { path: 'ngrx', loadChildren: './pizza/pizza-routing.module#PizzaRoutingModule' },
  { path: 'contentful', loadChildren: './contentful/contentful-routing.module#ContentfulRoutingModule' },
  { path: 'algolia', loadChildren: './algolia/algolia-routing.module#AlgoliaRoutingModule' },
  { path: 'graphql', loadChildren: './graphql/graphql-routing.module#GraphqlRoutingModule' },
  { path: 'geoquery', loadChildren: './geoquery/geoquery-routing.module#GeoqueryRoutingModule' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(globalRoutes, {preloadingStrategy: PreloadAllModules})
  ],
  declarations: [],
  exports: [RouterModule]
})
export class GlobalRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraphqlComponent } from './graphql.component';
import { RouterModule, Routes } from '@angular/router';
import { ApolloBoostModule, ApolloBoost } from 'apollo-angular-boost';
import { HttpClientModule } from '@angular/common/http';

const graphQlRoutes: Routes = [
  { path: '', component: GraphqlComponent },
];
@NgModule({
  imports: [
    CommonModule,
    ApolloBoostModule,
    HttpClientModule,
    RouterModule.forChild(graphQlRoutes)
  ],
  exports: [
    RouterModule,
    HttpClientModule,
    ApolloBoostModule
  ],
  declarations: [
    GraphqlComponent
  ]
})
export class GraphqlRoutingModule {
  constructor(apolloBoost: ApolloBoost) {
    apolloBoost.create({
      uri: 'http://localhost:4000'
    });
  }
}

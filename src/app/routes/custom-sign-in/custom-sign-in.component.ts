import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomAuthService } from '../../core/custom-auth-service/custom-auth.service';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'custom-sign-in',
  templateUrl: './custom-sign-in.component.html',
  styleUrls: ['./custom-sign-in.component.scss']
})
export class CustomSignInComponent implements OnInit {

  signupForm: FormGroup;
  signupEmail = new FormControl('', [ Validators.required, Validators.email ]);
  signupPassword = new FormControl('', [ Validators.required, Validators.minLength(6) ]);
  signupSubmitted = false;
  signInForm: FormGroup;
  signInEmail = new FormControl('', [ Validators.required, Validators.email ]);
  signInPassword = new FormControl('', [ Validators.required, Validators.minLength(6) ]);
  constructor(public customAuthService: CustomAuthService) { }

  ngOnInit() {
    this.signupForm = new FormGroup({
      signupEmail:  this.signupEmail,
      signupPassword:  this.signupPassword
    });
    this.signInForm = new FormGroup({
      signInEmail:  this.signInEmail,
      signInPassword:  this.signInPassword
    });
  }

  onSignup() {
    this.signupSubmitted = true;
    this.customAuthService.customSignup(this.signupForm.value.signupEmail, this.signupForm.value.signupPassword);
    // this.customAuthService.creationStatus.subscribe((value) => {
    //   console.log(value);
    // });
    this.signupEmail.reset();
    this.signupEmail.setValue('');
    this.signupPassword.reset();
    this.signupPassword.setValue('');
  }

  onSignIn() {
    this.customAuthService.customSignIn(this.signInForm.value.signInEmail, this.signInForm.value.signInPassword);
    this.signInEmail.reset();
    this.signInEmail.setValue('');
    this.signInPassword.reset();
    this.signInPassword.setValue('');
  }
}

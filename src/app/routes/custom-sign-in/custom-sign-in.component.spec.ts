import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomSignInComponent } from './custom-sign-in.component';

describe('CustomSignInComponent', () => {
  let component: CustomSignInComponent;
  let fixture: ComponentFixture<CustomSignInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomSignInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomSignInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { CustomSignInComponent } from './custom-sign-in.component';
import { MatFormFieldModule, MatInputModule } from '@angular/material';

const customSignInRoutes: Routes = [
  { path: '', component: CustomSignInComponent },
];

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    RouterModule.forChild(customSignInRoutes),
    ReactiveFormsModule
  ],
  exports: [
    RouterModule
  ],
  declarations: [
    CustomSignInComponent
  ]
})
export class CustomSignInRoutingModule { }

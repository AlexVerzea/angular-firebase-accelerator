import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AlgoliaComponent } from './algolia.component';
import { NgAisModule } from 'angular-instantsearch';

const algoliaRoutes: Routes = [
  { path: '', component: AlgoliaComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(algoliaRoutes),
    NgAisModule
  ],
  exports: [
    RouterModule
  ],
  declarations: [
    AlgoliaComponent
  ]
})
export class AlgoliaRoutingModule { }

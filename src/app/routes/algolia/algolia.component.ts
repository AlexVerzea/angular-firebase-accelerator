import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'algolia',
  templateUrl: './algolia.component.html',
  styleUrls: ['./algolia.component.scss']
})
export class AlgoliaComponent implements OnInit {

  searchConfig = {
    ...environment.algolia,
    indexName: 'pizza_search',
    routing: true
  };
  showResults = false;

  constructor() { }

  ngOnInit() {}

  searchChanged(query) {
    if (query.length) {
      this.showResults = true;
    } else {
      this.showResults = false;
    }
  }
}

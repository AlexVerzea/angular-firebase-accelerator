import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ContentfulComponent } from './contentful.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedMarkupModule } from '../../shared-markup/shared-markup.module';

const contentfulRoutes: Routes = [
  { path: '', component: ContentfulComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(contentfulRoutes),
    NgxPaginationModule,
    SharedMarkupModule
  ],
  exports: [
    RouterModule
  ],
  declarations: [
    ContentfulComponent
  ]
})
export class ContentfulRoutingModule { }

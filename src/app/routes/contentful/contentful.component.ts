import { Component, OnInit } from '@angular/core';
import { ContentfulService } from '../../core/contentful/contentful.service';
import { LanguageService } from '../../core/contentful/language.service';
import { Entry } from 'contentful';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-contentful',
  templateUrl: './contentful.component.html',
  styleUrls: ['./contentful.component.scss']
})
export class ContentfulComponent implements OnInit {

  constructor(
    private contentfulService: ContentfulService,
    private languageService: LanguageService,
    public sanitizer: DomSanitizer
  ) { }

  public selectedLocale = 'en-US';
  contentfulPage: Entry<any>;

  ngOnInit() {
    this.loadContent();
  }

  // Load the content from Contentful.
  loadContent() {
    this.languageService.events$.subscribe(locale => {
      this.contentfulService.getPageEntry('contentfulPage', locale)
      .then(entry => {
        entry.fields.pizzasInfo.forEach((pizzaInfo) => {
          pizzaInfo.url = this.sanitizer.bypassSecurityTrustStyle(`url(${pizzaInfo.url})`);
        });
        this.contentfulPage = entry;
      });
    });
  }

  // Set Language Toggle.
  setLanguageToggle(event) {
    this.selectedLocale = event.target.textContent.includes('English') || event.target.textContent.includes('Anglais') ? 'fr' : 'en-US';
    this.languageService.setLocale(this.selectedLocale);
  }

}

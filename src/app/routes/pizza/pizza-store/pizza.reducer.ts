import * as actions from './pizza.actions';
import { State, initialState, pizzaAdapter } from './pizza.selectors';

export function pizzaReducer(
    state: State = initialState,
    action: actions.PizzaActions) {
    switch (action.type) {
        case actions.ADDED:
            return pizzaAdapter.addOne(action.payload, state);
        case actions.MODIFIED:
            return pizzaAdapter.updateOne({
                id: action.payload.id,
                changes: action.payload
            }, state);
        case actions.REMOVED:
            return pizzaAdapter.removeOne(action.payload.id, state);
        default:
            return state;
    }
}

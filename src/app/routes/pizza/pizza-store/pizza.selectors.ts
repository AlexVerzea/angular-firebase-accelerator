import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';

// Definition of State and setting of the initial state.
export interface Pizza {
    id: string;
    size: string;
    status: string; // cooking or delivered
}
export const pizzaAdapter = createEntityAdapter<Pizza>();
export interface State extends EntityState<Pizza> { }
export const initialState: State = pizzaAdapter.getInitialState();

// Creation of Selectors.
export const getPizzaState = createFeatureSelector<State>('pizza');
export const { selectIds, selectEntities, selectAll, selectTotal } = pizzaAdapter.getSelectors(getPizzaState);

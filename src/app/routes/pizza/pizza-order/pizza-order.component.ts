import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as actions from '../pizza-store/pizza.actions';
import * as fromPizza from '../pizza-store/pizza.selectors';
import { MatChipsModule } from '@angular/material/chips';

@Component({
  selector: 'pizza-order',
  templateUrl: './pizza-order.component.html',
  styleUrls: ['./pizza-order.component.scss']
})
export class PizzaOrderComponent implements OnInit {

  pizzas: Observable<any>;

  constructor(private store: Store<fromPizza.State>) { }

  ngOnInit() {
    this.pizzas = this.store.select(fromPizza.selectAll);
    this.store.dispatch(new actions.Query());
  }

  updatePizza(id, status) {
    this.store.dispatch(new actions.Update(id, {status}));
  }
}

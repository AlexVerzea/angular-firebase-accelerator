import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PizzaOrderComponent } from './pizza-order/pizza-order.component';
import { StoreModule } from '@ngrx/store';
import { pizzaReducer } from './pizza-store/pizza.reducer';
import { PizzaEffects } from './pizza-store/pizza.effects';
import { EffectsModule } from '@ngrx/effects';
import { RouterModule, Routes } from '@angular/router';

const pizzaRoutes: Routes = [
  { path: '', component: PizzaOrderComponent },
];

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('pizza', pizzaReducer),
    EffectsModule.forFeature([PizzaEffects]),
    RouterModule.forChild(pizzaRoutes)
  ],
  declarations: [
    PizzaOrderComponent
  ],
  exports: [
    RouterModule
  ]
})
export class PizzaRoutingModule { }

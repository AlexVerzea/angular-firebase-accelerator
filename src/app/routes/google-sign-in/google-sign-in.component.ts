import { Component, OnInit } from '@angular/core';
import { GoogleAuthService } from '../../core/google-auth-service/google-auth.service';

@Component({
  selector: 'google-sign-in',
  templateUrl: './google-sign-in.component.html',
  styleUrls: ['./google-sign-in.component.scss']
})
export class GoogleSignInComponent implements OnInit {

  constructor(public googleAuthService: GoogleAuthService) { }
  ngOnInit() {}

}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoogleSignInComponent } from './google-sign-in.component';
import { RouterModule, Routes } from '@angular/router';

const googleSignInRoutes: Routes = [
  { path: '', component: GoogleSignInComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(googleSignInRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: [
    GoogleSignInComponent
  ]
})
export class GoogleSignInRoutingModule { }

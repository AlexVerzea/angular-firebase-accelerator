import { Component, OnInit } from '@angular/core';
import { FirestoreCommunicationService } from './core/firestore-communication/firestore-communication.service';
import { GoogleAuthService } from './core/google-auth-service/google-auth.service';
import { CustomAuthService } from './core/custom-auth-service/custom-auth.service';
import { filter, take } from 'rxjs/operators';
import {SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    public firestoreCommunicationService: FirestoreCommunicationService,
    public googleAuthService: GoogleAuthService,
    public customAuthService: CustomAuthService,
    private snotifyService: SnotifyService
  ) {}

  // Notifications when the app is open and in focus.
  style = 'material';
  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: true,
        maxAtPosition: 6,
        maxOnScreen: 8
      }
    });
    return {
      bodyMaxLength: 80,
      titleMaxLength: 15,
      backdrop: -1,
      position: SnotifyPosition.rightTop,
      timeout: 10000,
      showProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true
    };
  }

  ngOnInit() {
    this.googleAuthService.user
    .pipe(filter(user => !!user)) // filter null
    .pipe(take(1)) // take first real user
    .subscribe(user => {
      if (user) {
        this.firestoreCommunicationService.getPermission(user, 'google');
        this.firestoreCommunicationService.monitorRefresh(user, 'google');
        this.firestoreCommunicationService.receiveMessages();
      }
    });
    this.firestoreCommunicationService.currentMessage.subscribe(currentMessage => {
      if (currentMessage) {
        this.snotifyService.clear();
        this.snotifyService.success(currentMessage.notification.body, currentMessage.notification.title, this.getConfig());
      }
    });
    this.customAuthService.user
    .pipe(filter(user => !!user)) // filter null
    .pipe(take(1)) // take first real user
    .subscribe(user => {
      if (user) {
        this.firestoreCommunicationService.getPermission(user, 'custom');
        this.firestoreCommunicationService.monitorRefresh(user, 'custom');
        this.firestoreCommunicationService.receiveMessages();
      }
    });
    this.firestoreCommunicationService.currentMessage.subscribe(currentMessage => {
      if (currentMessage) {
        this.snotifyService.clear();
        this.snotifyService.success(currentMessage.notification.body, currentMessage.notification.title, this.getConfig());
      }
    });
  }
}

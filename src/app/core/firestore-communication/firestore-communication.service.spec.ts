import { TestBed, inject } from '@angular/core/testing';
import { FirestoreCommunicationService } from './firestore-communication.service';

describe('FirestoreCommunicationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FirestoreCommunicationService]
    });
  });

  it('should be created', inject([FirestoreCommunicationService], (service: FirestoreCommunicationService) => {
    expect(service).toBeTruthy();
  }));
});

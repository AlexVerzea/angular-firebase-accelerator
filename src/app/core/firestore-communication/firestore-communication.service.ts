import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class FirestoreCommunicationService {

  private messaging = firebase.messaging();
  currentMessage = new BehaviorSubject(null);

  constructor(private angularFirestore: AngularFirestore) {}

  // Save the permission token in firestore.
  private saveToken(user, token, service): void {
    const currentTokens = user.fcmTokens || { };
    const collection = service === 'google' ? 'users_google' : 'users_custom';

    // If token does not exist in firestore, update db.
    if (!currentTokens[token]) {
      const userRef = this.angularFirestore.collection(collection).doc(user.uid);
      const tokens = { ...currentTokens, [token]: true };
      userRef.update({ fcmTokens: tokens });
    }
  }

  // Get permission to send messages.
  getPermission(user, service) {
    this.messaging.requestPermission()
    .then(() => {
      return this.messaging.getToken();
    })
    .then(token => {
      this.saveToken(user, token, service);
    })
    .catch((err) => {
      console.log('Unable to get permission to notify.', err);
    });
  }

  // Listen for token refreshes.
  monitorRefresh(user, service) {
    this.messaging.onTokenRefresh(() => {
      this.messaging.getToken()
      .then(refreshedToken => {
        this.saveToken(user, refreshedToken, service);
      })
      .catch( err => console.log(err, 'Unable to retrieve new token'));
    });
  }

  // Return message contents when the app is open and focused.
  receiveMessages() {
    this.messaging.onMessage((payload) => {
      this.currentMessage.next(payload);
    });
  }
}

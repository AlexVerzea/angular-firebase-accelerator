import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import * as geofirex from 'geofirex';

@Injectable({
  providedIn: 'root'
})
export class GeofireService {

  constructor() { }

  geofireInit() {
    return geofirex.init(firebase);
  }
}

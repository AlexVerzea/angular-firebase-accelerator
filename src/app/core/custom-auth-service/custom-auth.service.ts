import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

interface CustomUser {
  uid: string;
  email: string;
  groups: string[];
  likes: string;
  fcmTokens?: { [token: string]: true };
}

@Injectable()
export class CustomAuthService {

  user: Observable<CustomUser>;
  creationStatus = of(false);

  constructor(private angularFireAuth: AngularFireAuth, private angularFirestore: AngularFirestore) {

    // Get auth data, then get firestore user document || null
    this.user = this.angularFireAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.angularFirestore.doc<CustomUser>(`users_custom/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
  }

  customSignup(email: string, password: string) {
    this.angularFirestore.collection('users_custom', ref => ref.where('email', '==', email))
      .valueChanges().subscribe((result) => {
        if (result.length === 0) {
          this.angularFireAuth.auth.createUserWithEmailAndPassword(email, password)
            .then((user) => {
              this.creationStatus = of(true);
              this.updateUserData(user);
            })
            .catch((error) => {
              console.log(error);
            });
          }
      });
  }

  customSignIn(email: string, password: string) {
    return this.angularFireAuth.auth.signInWithEmailAndPassword(email, password)
      .then((user) => {
        this.updateUserData(user);
      })
      .catch(error => console.log(error));
  }

  // Sets user data to firestore on login
  private updateUserData(user) {
    const userRef: AngularFirestoreDocument<CustomUser> = this.angularFirestore.doc(`users_custom/${user.user.uid}`);
    const data: CustomUser = {
      uid: user.user.uid,
      email: user.user.email,
      likes: '199',
      groups: ['1']
    };
    return userRef.set(data);
  }

  signOut() {
    this.angularFireAuth.auth.signOut();
  }
}

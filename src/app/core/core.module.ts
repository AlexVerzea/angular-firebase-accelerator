import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { environment } from '../../environments/environment';

// MDB Free Version
import { MDBBootstrapModule } from 'angular-bootstrap-md';

// AngularFire2
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FirestoreCommunicationService } from './firestore-communication/firestore-communication.service';

// Google Auth
import { GoogleAuthService } from './google-auth-service/google-auth.service';

// Custom Auth
import { CustomAuthService } from './custom-auth-service/custom-auth.service';

// Contentful
import { ContentfulService } from './contentful/contentful.service';
import { LanguageService } from './contentful/language.service';

// Geofire
import { GeofireService } from './geofire/geofire.service';

@NgModule({
  imports: [
    MDBBootstrapModule.forRoot(),
    AngularFireModule.initializeApp(environment.firestoreConfig), // firebase/app, needed for everything
    AngularFireDatabaseModule, // firebase/database, needed for db queries
    AngularFirestoreModule.enablePersistence(), // firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
  ],
  providers: [
    GoogleAuthService,
    CustomAuthService,
    ContentfulService,
    LanguageService,
    FirestoreCommunicationService,
    GeofireService
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class CoreModule { }

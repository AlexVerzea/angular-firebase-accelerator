import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class LanguageService {
  private ENGLISH = 'en-US';
  private locale = 'en-US';
  private changeLocale = new BehaviorSubject<String>(this.ENGLISH);

  setLocale(locale) {
    this.locale = locale;
    this.changeLocale.next(this.locale);
  }
  getLocale() {
    return this.locale;
  }
  get events$ () {
    return this.changeLocale.asObservable();
  }
}

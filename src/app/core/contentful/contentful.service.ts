import { Injectable } from '@angular/core';
import {Asset, createClient as cdaCreateClient, Entry, EntryCollection} from 'contentful';
import {createClient as cmaCreateClient, Space} from 'contentful-management';

// Content Delivery API Config
const cdaCONFIG = {
  space: 'cvtcoacydje5',
  accessToken: 'f909f88e7aa6fcef4ba6dc04f96926b01411e8e22e2a6b532e5389b83f2d5ed7',

  // Contentful Page
  contentfulPage: '6mD0VxRqhymSemC0gm8euY',
};

// Content Management API Config
const cmaCONFIG = {
  space: 'cvtcoacydje5',
  accessToken: 'CFPAT-CFPAT-c2a350b524c04e977be6f378c76d023535add50108f2eff217cbccb129efa054',
};

@Injectable()
export class ContentfulService {

  constructor() {}

  private cdaClient = cdaCreateClient({
    space: cdaCONFIG.space,
    accessToken: cdaCONFIG.accessToken
  });

  // There is a bug with cmaCreateClient. It is not necessary for this demo, so I comment it out.
  // private cmaClient = cmaCreateClient({
  //   accessToken: cmaCONFIG.accessToken
  // });

  getPageEntry(component: string, locale: String): Promise<Entry<any>> {
    return this.cdaClient.getEntry(cdaCONFIG[component], { 'locale' : locale });
  }

  getSearchResults(component: string, criteria: string, locale: String, order: string): Promise<EntryCollection<any>> {
    return this.cdaClient.getEntries({
      'content_type': component,
      'query': criteria,
      'order': order,
      'locale': locale
    });
  }

  getAsset (assetID: string): Promise<Asset> {
    return this.cdaClient.getAsset(assetID);
  }

  // Depends on cmaCreateClient. Also unnecessary for the demo and also commented out.
  // uploadPicture(fileTitle: String, fileData: String) {
  //   this.cmaClient.getSpace(cmaCONFIG.space)
  //     .then((space: Space) => space.createAsset({
  //       fields: {
  //         title: {
  //           'en-US': fileTitle,
  //           'fr': fileTitle
  //         },
  //         description: {
  //           'en-US': 'Profile Images',
  //           'fr': 'Profile Images'
  //         },
  //         file: {
  //           'en-US': {
  //             fileName: fileTitle,
  //             upload: fileData
  //           },
  //           'fr': {
  //             fileName: fileTitle,
  //             upload: fileData
  //           }
  //         }
  //       }
  //     }))
  //     .then((asset) => asset.processForAllLocales());
  // }
}

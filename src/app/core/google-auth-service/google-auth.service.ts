import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

interface GoogleUser {
  uid: string;
  email: string;
  photoURL?: string;
  displayName?: string;
  groups: string[];
  fcmTokens?: { [token: string]: true };
}

@Injectable()
export class GoogleAuthService {

  user: Observable<GoogleUser>;

  constructor(private angularFireAuth: AngularFireAuth, private angularFirestore: AngularFirestore) {

      // Get auth data, then get firestore user document || null
      this.user = this.angularFireAuth.authState.pipe(
        switchMap(user => {
          if (user) {
            return this.angularFirestore.doc<GoogleUser>(`users_google/${user.uid}`).valueChanges();
          } else {
            return of(null);
          }
        })
      );
  }

  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  private oAuthLogin(provider) {
    return this.angularFireAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        this.updateUserData(credential.user);
      });
  }

  // Sets user data to firestore on login
  private updateUserData(user) {
    const userRef: AngularFirestoreDocument<GoogleUser> = this.angularFirestore.doc(`users_google/${user.uid}`);
    const data: GoogleUser = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      groups: ['1'],
      photoURL: user.photoURL
    };
    return userRef.set(data);
  }

  signOut() {
    this.angularFireAuth.auth.signOut();
  }
}

import * as pizzaSelector from '../routes/pizza/pizza-store/pizza.selectors';

export interface AppState {
    pizza: pizzaSelector.State;
}

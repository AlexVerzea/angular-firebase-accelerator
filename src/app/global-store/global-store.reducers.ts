import * as pizzaReducer from '../routes/pizza/pizza-store/pizza.reducer';
import { AppState } from './global-store.selectors';
import { ActionReducerMap } from '@ngrx/store';

export const reducers: ActionReducerMap<AppState> = {
    pizza: pizzaReducer.pizzaReducer
};

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { environment } from '../environments/environment';
import { GlobalRoutingModule } from './routes/global-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// PWA
import { ServiceWorkerModule } from '@angular/service-worker';
import {SnotifyModule, SnotifyService, ToastDefaults} from 'ng-snotify';

// Core
import { CoreModule } from './core/core.module';

// NGRX
import { GlobalStoreModule } from './global-store/global-store.module';

// Header
import { HeaderModule } from './header/header.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CoreModule,
    HeaderModule,
    GlobalStoreModule,
    GlobalRoutingModule,
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
    SnotifyModule
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults},
    SnotifyService
  ]
})
export class AppModule { }

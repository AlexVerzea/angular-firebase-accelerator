import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarkdownToHtmlPipe } from './markdown-to-html.pipe';
import { SpaLinkTransformerDirective } from './spa-link-transformer.directive';

// Exports all Directives, Components and Pipes that must be available in multiple places in the application.
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    MarkdownToHtmlPipe,
    SpaLinkTransformerDirective
  ],
  exports: [
    MarkdownToHtmlPipe,
    SpaLinkTransformerDirective
  ]
})
export class SharedMarkupModule { }

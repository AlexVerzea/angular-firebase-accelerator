import {Directive, ElementRef, HostListener} from '@angular/core';
import {Router} from '@angular/router';

@Directive({
  selector: '[appSpaLinkTransformer]'
})
export class SpaLinkTransformerDirective {

  constructor(private el: ElementRef, private router: Router) { }

  @HostListener('click', ['$event'])
  public onClick(event) {
    if (event.target.hasAttribute('href')) {
      if (event.target.getAttribute('href').indexOf('https://spa-link/') !== -1) {
        this.router.navigate([event.target.getAttribute('href').replace('https://spa-link/', '')]);
        event.preventDefault();
      } else {
        if (event.target.getAttribute('href').indexOf('mailto') === -1) {
          event.target.setAttribute('target', '_blank');
        }
        return;
      }
    }
  }
}

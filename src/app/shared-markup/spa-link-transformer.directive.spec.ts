import { SpaLinkTransformerDirective } from './spa-link-transformer.directive';
import { Component } from '@angular/core';
import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {Routes} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {Location} from '@angular/common';

@Component({
  template: `<div appSpaLinkTransformer><a href='https://spa-link/dummy-component'>schedule</a></div>`
})
class TestSpaLinkTransformerComponent {
}

@Component({
  template: `<p>Dummy Component</p>`
})
class DummyRoutingComponent {}

export const routes: Routes = [
  { path: 'dummy-component', component: DummyRoutingComponent }
];

describe('SpaLinkTransformerDirective', () => {

  let fixture: ComponentFixture<TestSpaLinkTransformerComponent>;
  let location: Location;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      declarations: [SpaLinkTransformerDirective, TestSpaLinkTransformerComponent, DummyRoutingComponent]
    });
    location = TestBed.get(Location);
  });

  it('should reroute to the Dummy Component', fakeAsync( () => {
    fixture = TestBed.createComponent(TestSpaLinkTransformerComponent);
    const a = fixture.debugElement.query(By.css('a')).nativeElement;
    a.click();
    tick();
    fixture.detectChanges();
    expect(location.path()).toBe('/dummy-component');
  }));
});

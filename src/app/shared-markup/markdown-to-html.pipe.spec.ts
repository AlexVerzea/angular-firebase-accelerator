import { MarkdownToHtmlPipe } from './markdown-to-html.pipe';

describe('MarkdownToHtmlPipe', () => {
  it('should convert the Markdown to HTML', () => {
    const pipe = new MarkdownToHtmlPipe();
    const inputExample = '<a></a>';
    expect(pipe).toBeTruthy();
    expect(pipe.transform(inputExample)).toContain('<p><a></a></p>');
  });
});

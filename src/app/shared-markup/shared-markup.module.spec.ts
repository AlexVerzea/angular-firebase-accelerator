import { SharedMarkupModule } from './shared-markup.module';

describe('SharedMarkupModule', () => {
  let sharedMarkupModule: SharedMarkupModule;

  beforeEach(() => {
    sharedMarkupModule = new SharedMarkupModule();
  });

  it('should create an instance', () => {
    expect(sharedMarkupModule).toBeTruthy();
  });
});

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firestoreConfig: {
    apiKey: 'AIzaSyCUqiEDIc0VbM-xBMNVM8fnUoZZyOSE8AI',
    authDomain: 'fir-starter-6ab95.firebaseapp.com',
    databaseURL: 'https://fir-starter-6ab95.firebaseio.com',
    projectId: 'fir-starter-6ab95',
    storageBucket: 'fir-starter-6ab95.appspot.com',
    messagingSenderId: '1074179928649'
  },
  algolia: {
    appId: 'SQXOERWPVK',
    apiKey: '7e03aabd017f3ace535a0480a1068054'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

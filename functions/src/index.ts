import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as algoliasearch from 'algoliasearch';
import { ApolloServer, ApolloError, ValidationError, gql } from 'apollo-server';

const serviceAccount = require('../service-account.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});
const db = admin.firestore();
const client = algoliasearch('SQXOERWPVK', '7e03aabd017f3ace535a0480a1068054');
const index = client.initIndex('pizza_search');

exports.targetSpecificUser = functions.firestore.document('messages_one/{messageId}').onCreate((snap, context) => {

    const message = snap.data();
    const userId = message.recipientId;
    const payload = {
        notification: {
            title: message.title,
            body: message.body,
            icon: 'favicon.ico',
            badge: 'favicon.ico'
        }
    }

    // For Push Notifs through Google Sign In, use the collection users_google.
    const userRef = db.collection('users_custom').doc(userId);
    return userRef.get()
        .then(snapshot => snapshot.data())
        .then(user => {
            const tokens = user.fcmTokens ? Object.keys(user.fcmTokens) : [];
            if (!tokens.length) {
                throw new Error('User [' + userId + ']: No tokens found!');
            }
            console.log('User [' + userId + ']: ' + tokens.length + ' token(s) found!');
            return admin.messaging().sendToDevice(tokens, payload);
        })
        .catch(error => console.log(error));
});

exports.targetUserGroup = functions.firestore.document('messages_group/{messageId}').onCreate(async (snap, context) => {

    const message = snap.data();
    const groupId = message.groupId;
    const payload = {
        notification: {
            title: message.title,
            body: message.body,
            icon: 'favicon.ico',
            badge: 'favicon.ico'
        }
    }
    const groupTokens = [];

    // For Push Notifs through Google Sign In, use the collection users_google.
    const users = await db.collection('users_custom').get();
    users.forEach(user => {
        const userData = user.data();
        const userGroups = userData.groups ? userData.groups : [];
        const userTokens = userData.fcmTokens ? Object.keys(userData.fcmTokens) : [];
        if (userTokens.length && userGroups.indexOf(groupId) !== -1) {
            userTokens.forEach((token) => {
                groupTokens.push(token);
            });
        }
    });
    console.log('Group [' + groupId + ']: ' + groupTokens.length + ' user token(s) found!');
    return admin.messaging().sendToDevice(groupTokens, payload).catch(error => console.log(error));
});

exports.targetAllUsers = functions.firestore.document('messages_all/{messageId}').onCreate(async (snap, context) => {

    const message = snap.data();
    const payload = {
        notification: {
            title: message.title,
            body: message.body,
            icon: 'favicon.ico',
            badge: 'favicon.ico'
        }
    }
    const allTokens = [];

    // For Push Notifs through Google Sign In, use the collection users_google.
    const users = await db.collection('users_custom').get();
    users.forEach(user => {
        const userData = user.data();
        const userTokens = userData.fcmTokens ? Object.keys(userData.fcmTokens) : [];
        if (userTokens.length) {
            userTokens.forEach((token) => {
                allTokens.push(token);
            });
        }
    });
    console.log('All: ' + allTokens.length + ' user token(s) found overall!');
    return admin.messaging().sendToDevice(allTokens, payload).catch(error => console.log(error));
});

exports.targetUserOutsideArea = functions.firestore.document('positions/{positionId}').onCreate(async (snap, context) => {

    const message = snap.data();
    const userId = message.recipientId;
    const isInRadius = message.isInRadius;
    const payload = {
        notification: {
            title: 'GeoQuery!',
            body: 'Target has left the area!',
            icon: 'favicon.ico',
            badge: 'favicon.ico'
        }
    }

    if (!isInRadius) {
        console.log("Target is outside the area!");
        const userRef = db.collection('users_custom').doc(userId);
        return userRef.get()
            .then(snapshot => snapshot.data())
            .then(user => {
                const tokens = user.fcmTokens ? Object.keys(user.fcmTokens) : [];
                if (!tokens.length) {
                    throw new Error('User [' + userId + ']: No tokens found!');
                }
                console.log('User [' + userId + ']: ' + tokens.length + ' token(s) found!');
                return admin.messaging().sendToDevice(tokens, payload);
            })
            .catch(error => console.log(error));
    } else {
        console.log("Target is inside the area!");
    }
});

// Index pizza to Algolia upon creation.
exports.indexPizza = functions.firestore.document('pizzas/{pizzaId}').onCreate((snap, context) => {

    const data = snap.data();
    const objectID = snap.id;
    return index.addObject({
        objectID,
        ...data
    });
});

// Unindex pizza to Algolia upon deletion.
exports.unindexPizza = functions.firestore.document('pizzas/{pizzaId}').onDelete((snap, context) => {

    const objectId = snap.id;
    return index.deleteObject(objectId);
});

interface User {
    uid: string;
    email: string;
    likes: number;
}

interface Tweet {
    id: string;
    text: string;
    likes: number;
    uid: string;
}

/**
 * Type Definitions: First piece of the GraphQL API. Where we write actual GraphQL code to define the shape of the data
 * available on the server.
 * N.B.: The ! signifies that this property is guaranteed to not be null. [] is for arrays.
 */
const typeDefs = gql`
  # A Twitter User
  type User {
    uid: ID!
    email: String!
    likes: Int!,
    groups: [String]!,
    tweets: [Tweet]!
  }

  # A Tweet Object
  type Tweet {
    id: ID!
    text: String!
    user: User!
    likes: Int!
  }

  type Query {
    tweets: [Tweet]
    user(uid: String!): User
  }

  type Mutation {
    likeTweet(id: ID!): Tweet
  }
`;

/**
 * Resolvers: Second piece of the GraphQL API. Tell GraphQL how to return the data to the client through the API.
 */
const resolvers = {
    User: {
        async tweets(user) {
            try {
                const userTweets = await admin
                    .firestore()
                    .collection('tweets')
                    .where('uid', '==', user.uid)
                    .get();
                return userTweets.docs.map(tweet => tweet.data()) as Tweet[];
            } catch (error) {
                throw new ApolloError(error);
            }
        }
    },
    Tweet: {
        async user(tweet) {
            try {
                const tweetAuthor = await admin
                    .firestore()
                    .doc(`users_custom/${tweet.uid}`)
                    .get();
                return tweetAuthor.data() as User;
            } catch (error) {
                throw new ApolloError(error);
            }
        }
    },
    Query: {
        async tweets() {
            const tweets = await admin
                .firestore()
                .collection('tweets')
                .get();
            return tweets.docs.map(tweet => tweet.data()) as Tweet[];
        },
        async user(_: null, args: { uid: string }) {
            try {
                const userDoc = await admin
                    .firestore()
                    .doc(`users_custom/${args.uid}`)
                    .get();
                const user = userDoc.data() as User | undefined;
                return user || new ValidationError('User ID not found');
            } catch (error) {
                throw new ApolloError(error);
            }
        }
    },
    Mutation: {
        likeTweet: async (_, args: { id: string }) => {
            try {
                const tweetRef = admin.firestore().doc(`tweets/${args.id}`);

                // Increment likes on a tweet. In real life we would need to create a transaction here!
                let tweetDoc = await tweetRef.get();
                const tweet = tweetDoc.data() as Tweet;
                await tweetRef.update({ likes: tweet.likes + 1 });
                tweetDoc = await tweetRef.get();
                return tweetDoc.data();
            } catch (error) {
                throw new ApolloError(error);
            }
        }
    }
};

// Pass the Type Definitions and Resolvers to the Apollo Server.
const server = new ApolloServer({
    typeDefs,
    resolvers,
    engine: {
        apiKey: "service:AlexVerzeaINM-9091:4c0-cgic0N6-sBj-kKxKzA"
    },
    introspection: true
});

server
    .listen({ port: process.env.PORT || 4000 })
    .then(({ url }) => {
        console.log(`🚀  Server ready at ${url}`);
    })
    .catch(() => {
        console.log("Server failed to load!");
    });

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const algoliasearch = require("algoliasearch");
const apollo_server_1 = require("apollo-server");
const serviceAccount = require('../service-account.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});
const db = admin.firestore();
const client = algoliasearch('SQXOERWPVK', '7e03aabd017f3ace535a0480a1068054');
const index = client.initIndex('pizza_search');
exports.targetSpecificUser = functions.firestore.document('messages_one/{messageId}').onCreate((snap, context) => {
    const message = snap.data();
    const userId = message.recipientId;
    const payload = {
        notification: {
            title: message.title,
            body: message.body,
            icon: 'favicon.ico',
            badge: 'favicon.ico'
        }
    };
    // For Push Notifs through Google Sign In, use the collection users_google.
    const userRef = db.collection('users_custom').doc(userId);
    return userRef.get()
        .then(snapshot => snapshot.data())
        .then(user => {
        const tokens = user.fcmTokens ? Object.keys(user.fcmTokens) : [];
        if (!tokens.length) {
            throw new Error('User [' + userId + ']: No tokens found!');
        }
        console.log('User [' + userId + ']: ' + tokens.length + ' token(s) found!');
        return admin.messaging().sendToDevice(tokens, payload);
    })
        .catch(error => console.log(error));
});
exports.targetUserGroup = functions.firestore.document('messages_group/{messageId}').onCreate((snap, context) => __awaiter(this, void 0, void 0, function* () {
    const message = snap.data();
    const groupId = message.groupId;
    const payload = {
        notification: {
            title: message.title,
            body: message.body,
            icon: 'favicon.ico',
            badge: 'favicon.ico'
        }
    };
    const groupTokens = [];
    // For Push Notifs through Google Sign In, use the collection users_google.
    const users = yield db.collection('users_custom').get();
    users.forEach(user => {
        const userData = user.data();
        const userGroups = userData.groups ? userData.groups : [];
        const userTokens = userData.fcmTokens ? Object.keys(userData.fcmTokens) : [];
        if (userTokens.length && userGroups.indexOf(groupId) !== -1) {
            userTokens.forEach((token) => {
                groupTokens.push(token);
            });
        }
    });
    console.log('Group [' + groupId + ']: ' + groupTokens.length + ' user token(s) found!');
    return admin.messaging().sendToDevice(groupTokens, payload).catch(error => console.log(error));
}));
exports.targetAllUsers = functions.firestore.document('messages_all/{messageId}').onCreate((snap, context) => __awaiter(this, void 0, void 0, function* () {
    const message = snap.data();
    const payload = {
        notification: {
            title: message.title,
            body: message.body,
            icon: 'favicon.ico',
            badge: 'favicon.ico'
        }
    };
    const allTokens = [];
    // For Push Notifs through Google Sign In, use the collection users_google.
    const users = yield db.collection('users_custom').get();
    users.forEach(user => {
        const userData = user.data();
        const userTokens = userData.fcmTokens ? Object.keys(userData.fcmTokens) : [];
        if (userTokens.length) {
            userTokens.forEach((token) => {
                allTokens.push(token);
            });
        }
    });
    console.log('All: ' + allTokens.length + ' user token(s) found overall!');
    return admin.messaging().sendToDevice(allTokens, payload).catch(error => console.log(error));
}));
exports.targetUserOutsideArea = functions.firestore.document('positions/{positionId}').onCreate((snap, context) => __awaiter(this, void 0, void 0, function* () {
    const message = snap.data();
    const userId = message.recipientId;
    const isInRadius = message.isInRadius;
    const payload = {
        notification: {
            title: 'GeoQuery!',
            body: 'Target has left the area!',
            icon: 'favicon.ico',
            badge: 'favicon.ico'
        }
    };
    if (!isInRadius) {
        console.log("Target is outside the area!");
        const userRef = db.collection('users_custom').doc(userId);
        return userRef.get()
            .then(snapshot => snapshot.data())
            .then(user => {
            const tokens = user.fcmTokens ? Object.keys(user.fcmTokens) : [];
            if (!tokens.length) {
                throw new Error('User [' + userId + ']: No tokens found!');
            }
            console.log('User [' + userId + ']: ' + tokens.length + ' token(s) found!');
            return admin.messaging().sendToDevice(tokens, payload);
        })
            .catch(error => console.log(error));
    }
    else {
        console.log("Target is inside the area!");
    }
}));
// Index pizza to Algolia upon creation.
exports.indexPizza = functions.firestore.document('pizzas/{pizzaId}').onCreate((snap, context) => {
    const data = snap.data();
    const objectID = snap.id;
    return index.addObject(Object.assign({ objectID }, data));
});
// Unindex pizza to Algolia upon deletion.
exports.unindexPizza = functions.firestore.document('pizzas/{pizzaId}').onDelete((snap, context) => {
    const objectId = snap.id;
    return index.deleteObject(objectId);
});
/**
 * Type Definitions: First piece of the GraphQL API. Where we write actual GraphQL code to define the shape of the data
 * available on the server.
 * N.B.: The ! signifies that this property is guaranteed to not be null. [] is for arrays.
 */
const typeDefs = apollo_server_1.gql `
  # A Twitter User
  type User {
    uid: ID!
    email: String!
    likes: Int!,
    groups: [String]!,
    tweets: [Tweet]!
  }

  # A Tweet Object
  type Tweet {
    id: ID!
    text: String!
    user: User!
    likes: Int!
  }

  type Query {
    tweets: [Tweet]
    user(uid: String!): User
  }

  type Mutation {
    likeTweet(id: ID!): Tweet
  }
`;
/**
 * Resolvers: Second piece of the GraphQL API. Tell GraphQL how to return the data to the client through the API.
 */
const resolvers = {
    User: {
        tweets(user) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const userTweets = yield admin
                        .firestore()
                        .collection('tweets')
                        .where('uid', '==', user.uid)
                        .get();
                    return userTweets.docs.map(tweet => tweet.data());
                }
                catch (error) {
                    throw new apollo_server_1.ApolloError(error);
                }
            });
        }
    },
    Tweet: {
        user(tweet) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const tweetAuthor = yield admin
                        .firestore()
                        .doc(`users_custom/${tweet.uid}`)
                        .get();
                    return tweetAuthor.data();
                }
                catch (error) {
                    throw new apollo_server_1.ApolloError(error);
                }
            });
        }
    },
    Query: {
        tweets() {
            return __awaiter(this, void 0, void 0, function* () {
                const tweets = yield admin
                    .firestore()
                    .collection('tweets')
                    .get();
                return tweets.docs.map(tweet => tweet.data());
            });
        },
        user(_, args) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const userDoc = yield admin
                        .firestore()
                        .doc(`users_custom/${args.uid}`)
                        .get();
                    const user = userDoc.data();
                    return user || new apollo_server_1.ValidationError('User ID not found');
                }
                catch (error) {
                    throw new apollo_server_1.ApolloError(error);
                }
            });
        }
    },
    Mutation: {
        likeTweet: (_, args) => __awaiter(this, void 0, void 0, function* () {
            try {
                const tweetRef = admin.firestore().doc(`tweets/${args.id}`);
                // Increment likes on a tweet. In real life we would need to create a transaction here!
                let tweetDoc = yield tweetRef.get();
                const tweet = tweetDoc.data();
                yield tweetRef.update({ likes: tweet.likes + 1 });
                tweetDoc = yield tweetRef.get();
                return tweetDoc.data();
            }
            catch (error) {
                throw new apollo_server_1.ApolloError(error);
            }
        })
    }
};
// Pass the Type Definitions and Resolvers to the Apollo Server.
const server = new apollo_server_1.ApolloServer({
    typeDefs,
    resolvers,
    engine: {
        apiKey: "service:AlexVerzeaINM-9091:4c0-cgic0N6-sBj-kKxKzA"
    },
    introspection: true
});
server
    .listen({ port: process.env.PORT || 4000 })
    .then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
})
    .catch(() => {
    console.log("Server failed to load!");
});
//# sourceMappingURL=index.js.map
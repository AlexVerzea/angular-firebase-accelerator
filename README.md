# AngularAccelerator

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Features

### Miscellaneous  

1. Angular default styling set to SCSS. SCSS theme structure is already created in the project. 
2. Angular Material and MDB-A (Free version) are installed and running.
3. TSLint is installed and running.
4. Preloading and Lazy-Loading of all Modules.
5. Uglify enabled: All JS and CSS files are minified. 
6. Node 8 is enabled for Google Cloud Functions (default is Node 6). This upgrade allows for the use of the latest features from Node, especially the useful `async` & `await` commands and the spread operator. For more details, see:  
  * Node 8 features: https://nodesource.com/blog/five-fantastic-features-shipping-with-node-js-8-lts-carbon/  
  * How to upgrade GCF to Node 8: https://howtofirebase.com/cloud-functions-migrating-to-node-8-9640731a8acc  

### Angular PWA  

1. Service Worker in `ngsw-config.json`  
2. Manifest in `manifest.json`  
3. App Shell in `index.html`  
4. Run as follows:  
  * Run `ng build --prod`  
  * Run `cd dist`  
  * Run `http-server`. For a full list of options, see: https://www.npmjs.com/package/http-server.  
  * Open Chrome in Incognito Mode and enter the url.  

### NGRX Pizza (with Firestore)  

1. State Management with NGRX: Implemented the Pizza App from AngularFirebase in `pizza-store`. This can be used as a blueprint for any state management work that involves Angular and Firestore. 
2. Can monitor all state changes, whether they come from the app or from Firestore, with Chrome Dev Tools.  
3. For more details, see: https://www.youtube.com/watch?v=rv37jBygQ2g  

### Custom Sign In (with Firestore)  

1. Implemented Auth (both Signup and Sign In) through Angular Reactive Forms in `custom-auth-service`. User credentials are stored in Firestore.
2. Only the base cases (Signup, Sign In) are implemented since the point here is only to demo the Auth capabilities of Firestore. No confirmation/failure messages exist. For a full implementation with such messages, NGRX should be used.

### Google Sign In (with Firestore)  

1. Implemented Auth through Google Sign In in `google-auth-service`. User credentials are stored in Firestore. 
2. For more details, see: https://www.youtube.com/watch?v=e8GA1UOj8mE&t=167s  

### Contentful Pizza  

1. Contentful 7.0.0 used for Content Infrastructure. 
2. Contentful communication handled by `contentful.service.ts`.  
3. i18n handled by `language.service.ts`.  
4. URL sanitization done through the `DomSanitizer`.  
5. For more details, see: https://www.youtube.com/watch?v=2agJoqviGY4  

### Push Notifications (with Firestore)  

1. Implemented Push Notifications for users whose credentials are already stored in Firestore (either through Google Sign In or Custom Sign In). 3 cases are implemented:  
  * Fire Notifications to a specific user.  
  * Fire Notifications to a group of users.  
  * Fire Notifications to all users.  
2. N.B.:  
  * The 3 cases above are implemented with Firestore Cloud Functions in `index.ts`.  
  * The retrieval of Notification contents on the Frontend is done through `firestore-communication.service.ts`.  
  * NG-Snotify is used to display the notification when the app is opened and in-focus: https://artemsky.github.io/ng-snotify/ 
3. For more details see: https://www.youtube.com/watch?v=SOOjamH1bAA  

### Algolia Pizza (with Firestore)  

1. Implemented Angular InstantSearch in `algolia.component.ts`.  
2. For more details, see:  
  * https://www.youtube.com/watch?v=3Z0V3cvgns8&t=2s  
  * https://community.algolia.com/angular-instantsearch/  

### GraphQL Tweets (with Apollo Angular Client, Apollo Engine, Apollo Server and Firestore)  

1. Can request data about Custom Users and Tweets in exactly the way it should be consumed in the Frontend. This is
the main difference with REST APIs, where data is typically queried in a convoluted fashion.  
2. GraphQL is very useful if we have relational data that we need to join from multiple sources, which is commonly the
case with NoSQL databases.  
3. Apollo Engine: Gives access to a dashboard where we can monitor Performance, Errors, etc about each Query in real-time.  
4. Can install Google CLI Tools to deploy the app to one of their production servers, making it available to the
whole world. Can then pass requests directly through Insomnia's GraphQL mode. This is beyond the scope of this
accelerator and it was not implemented.  
5. Apollo Angular Boost: Client library that makes it easy to setup GraphQL in an Angular app.  
6. Apollo Codegen: Allows us to generate TypeScript definitions from server-side automatically.  
7. Optimistic UI: Normally, updating the likes triggers a wait period until the server updates. But for a great UX, we want to update the UI immediately while the update happens through the network. Apollo can update the local store immediately with the object data, then when the server response comes, it will overwrite it in the store.  
8. Run `npm run serve` inside `functions` first to start the Apollo Server. Then run `ng serve` at the Angular level to 
start the client-side part of the app.  
9. For more details, see:  
  * https://www.youtube.com/watch?time_continue=161&v=8D9XnnjFGMs  
  * https://www.youtube.com/watch?time_continue=433&v=Wc7bJ2uv694  

### GeoQueries (with Firestore) -- IN PROGRESS!  

DONE:  

1. Angular Google Maps (AGM) used as the component library for generating the Google Map. GeoPoints stored in Firestore and a circular area are rendered on the map.  
2. Google's Maps JS API needs to be enabled to store and manipulate GeoPoints through Firestore.  
3. The pseudorandom map traversal function is implemented. It creates a test geopoint in Firestore and updates it's latitude, longitude and geohash on every iteration for a constant time period. The test GeoPoint has a `isInRadius` property, which is set to true prior to storage if the point has left the circular area.  
4. A Firestore cloud function listens for changes to the test GeoPoint. If it's `isInRadius` property is set to true, it fires a Push Notification, alerting the signed in user that the target has left the area.  

TODO:  

1. The accuracy of the calculation of the test GeoPoint's distance from the center is currently miserable and needs improvement. Commented out until fixed.  
2. Currently, the push notification is sent to a hardcoded user id. Need to change this to allow for a user id to be chosen.
3. Refactoring needed to pass all Frontend functionality into an Angular service.  
4. Need to find a way to test with real mobile phone data, stored and updated in Firebase through another Angular service.  

### Airtable  

1. Coming up.  

### Google Assistant (with Firestore)  

1. Coming up. 

### Cypress  

1. Coming up.  

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
